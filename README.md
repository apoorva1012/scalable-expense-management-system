#Assignment 3

### Steps ###

* Clone the repo
* Run docker-compose to run required containers
* Run the file app/ch_client.py locally
* POST the data on localhost:4000/v1/expenses
* Run app/test.py


##Response:

Server 127.0.0.1:5002 returns:

{
  "category": "test val1", 
  "decision_date": "12-17-2016", 
  "description": "test comments", 
  "email": "add@das1.com", 
  "estimated_costs": "222", 
  "id": 1, 
  "link": "http://www.asd.com", 
  "name": "app1", 
  "status": "pending", 
  "submit_date": "12-16-2016"
}

*************************

Server 127.0.0.1:5003 returns:

{
  "category": "test val1", 
  "decision_date": "12-17-2016", 
  "description": "test comments", 
  "email": "add@das1.com", 
  "estimated_costs": "222", 
  "id": 2, 
  "link": "http://www.asd.com", 
  "name": "app1", 
  "status": "pending", 
  "submit_date": "12-16-2016"
}

*************************

Server 127.0.0.1:5001 returns:

{
  "category": "test val1", 
  "decision_date": "12-17-2016", 
  "description": "test comments", 
  "email": "add@das1.com", 
  "estimated_costs": "222", 
  "id": 3, 
  "link": "http://www.asd.com", 
  "name": "app1", 
  "status": "pending", 
  "submit_date": "12-16-2016"
}

*************************

Server 127.0.0.1:5001 returns:

{
  "category": "test val1", 
  "decision_date": "12-17-2016", 
  "description": "test comments", 
  "email": "add@das1.com", 
  "estimated_costs": "222", 
  "id": 4, 
  "link": "http://www.asd.com", 
  "name": "app1", 
  "status": "pending", 
  "submit_date": "12-16-2016"
}

*************************

Server 127.0.0.1:5003 returns:

{
  "category": "test val1", 
  "decision_date": "12-17-2016", 
  "description": "test comments", 
  "email": "add@das1.com", 
  "estimated_costs": "222", 
  "id": 5, 
  "link": "http://www.asd.com", 
  "name": "app1", 
  "status": "pending", 
  "submit_date": "12-16-2016"
}

*************************

Server 127.0.0.1:5001 returns:

{
  "category": "test val1", 
  "decision_date": "12-17-2016", 
  "description": "test comments", 
  "email": "add@das1.com", 
  "estimated_costs": "222", 
  "id": 6, 
  "link": "http://www.asd.com", 
  "name": "app1", 
  "status": "pending", 
  "submit_date": "12-16-2016"
}

*************************

Server 127.0.0.1:5001 returns:

{
  "category": "test val1", 
  "decision_date": "12-17-2016", 
  "description": "test comments", 
  "email": "add@das1.com", 
  "estimated_costs": "222", 
  "id": 7, 
  "link": "http://www.asd.com", 
  "name": "app1", 
  "status": "pending", 
  "submit_date": "12-16-2016"
}

*************************

Server 127.0.0.1:5003 returns:

{
  "category": "test val1", 
  "decision_date": "12-17-2016", 
  "description": "test comments", 
  "email": "add@das1.com", 
  "estimated_costs": "222", 
  "id": 8, 
  "link": "http://www.asd.com", 
  "name": "app1", 
  "status": "pending", 
  "submit_date": "12-16-2016"
}

*************************

Server 127.0.0.1:5001 returns:

{
  "category": "test val1", 
  "decision_date": "12-17-2016", 
  "description": "test comments", 
  "email": "add@das1.com", 
  "estimated_costs": "222", 
  "id": 9, 
  "link": "http://www.asd.com", 
  "name": "app1", 
  "status": "pending", 
  "submit_date": "12-16-2016"
}

*************************

Server 127.0.0.1:5003 returns:

{
  "category": "test val1", 
  "decision_date": "12-17-2016", 
  "description": "test comments", 
  "email": "add@das1.com", 
  "estimated_costs": "222", 
  "id": 10, 
  "link": "http://www.asd.com", 
  "name": "app1", 
  "status": "pending", 
  "submit_date": "12-16-2016"
}

*************************

The summary is:
{'127.0.0.1:5002': 1, '127.0.0.1:5003': 4, '127.0.0.1:5001': 5}